import { IsNotEmpty, IsString } from 'class-validator';

export class SoccerLeagueDto {
  @IsString()
  @IsNotEmpty()
  name?: string;
}