import { Component, OnInit } from '@angular/core';
import { SoccerLeague } from '../../../models/soccer-league';
import { SoccerLeagueService } from '../../../services/soccer-league.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-soccer-league',
  templateUrl: './edit-soccer-league.component.html',
  styleUrls: ['./edit-soccer-league.component.css']
})
export class EditSoccerLeagueComponent implements OnInit {
  soccerLeague = new SoccerLeague('');

  constructor(private soccerLeagueService: SoccerLeagueService, private activatedRoute: ActivatedRoute, private toastrService: ToastrService, private router: Router) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.soccerLeagueService.getSoccerLeagueById(id).subscribe(
      data => {
        this.soccerLeague = data;
      },
      error => {
        this.toastrService.error(error.error.message, 'Fail', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      }
    );
  }

  updateSoccerLeague(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.soccerLeagueService.updateSoccerLeague(id, this.soccerLeague).subscribe(
      data => {
        this.toastrService.success('Liga de Fútbol actualizada.', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      error => {
        this.toastrService.error(error.error.message, 'Fail', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
      }
    );
  }

}
