import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SoccerLeague } from 'src/app/models/soccer-league';
import { SoccerLeagueService } from 'src/app/services/soccer-league.service';

@Component({
  selector: 'app-soccer-league-list',
  templateUrl: './soccer-league-list.component.html',
  styleUrls: ['./soccer-league-list.component.css']
})
export class SoccerLeagueListComponent implements OnInit {
  soccerLeagues: SoccerLeague[] = []!;

  constructor(private soccerLeagueService: SoccerLeagueService) { }

  ngOnInit(): void {
    this.getSoccerLeagues();
  }

  getSoccerLeagues(): void {
    this.soccerLeagueService.getSoccerLeagues().subscribe(
      data => {
        this.soccerLeagues = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  deleteSoccerLeague(): void {
    // console.log('Borrar', id);
  }

}
