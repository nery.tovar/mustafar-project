import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SoccerLeague } from '../models/soccer-league';

@Injectable({
  providedIn: 'root'
})
export class SoccerLeagueService {
  soccerLeagueEndpoint = `${environment.url}soccer-league`;

  constructor(private httpClient: HttpClient) { }

  public getSoccerLeagues(): Observable<SoccerLeague[]> {
    return this.httpClient.get<SoccerLeague[]>(this.soccerLeagueEndpoint);
  }

  public getSoccerLeagueById(id: number): Observable<SoccerLeague> {
    return this.httpClient.get<SoccerLeague>(`${this.soccerLeagueEndpoint}/${id}`);
  }

  public createSoccerLeague(soccerLeague: SoccerLeague): Observable<any> {
    return this.httpClient.post<any>(this.soccerLeagueEndpoint, soccerLeague);
  }

  public updateSoccerLeague(id: number, soccerLeague: SoccerLeague): Observable<any> {
    return this.httpClient.put<any>(`${this.soccerLeagueEndpoint}/${id}`, soccerLeague);
  }

  public deleteSoccerLeague(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${this.soccerLeagueEndpoint}/${id}`);
  }
}
