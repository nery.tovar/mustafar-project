import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import { SoccerLeagueService } from './soccer-league.service';
import { SoccerLeagueDto } from './dto/soccer-league.dto';

@Controller('soccer-league')
export class SoccerLeagueController {
  constructor(private readonly soccerLeagueService: SoccerLeagueService) {}

  @Get()
  async getSoccerLeagues() {
    return await this.soccerLeagueService.getSoccerLeagues();
  }

  @Get(':id')
  async getSoccerLeagueById(@Param('id', ParseIntPipe) id: number) {
    return await this.soccerLeagueService.getSoccerLeagueById(id);
  }

  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  async createSoccerLeague(@Body() dto: SoccerLeagueDto) {
    return await this.soccerLeagueService.createSoccerLeague(dto);
  }

  @UsePipes(new ValidationPipe({whitelist: true}))
  @Put(':id')
  async updateSoccerLeague(@Param('id', ParseIntPipe) id: number, @Body() dto: SoccerLeagueDto) {
    return await this.soccerLeagueService.updateSoccerLeague(id, dto);
  }

  @Delete(':id')
  async deleteSoccerLeague(@Param('id', ParseIntPipe) id: number) {
    return await this.soccerLeagueService.deleteSoccerLeague(id);
  }
}
