import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SoccerLeagueListComponent } from './components/soccer-league/soccer-league-list/soccer-league-list.component';
import { NewSoccerLeagueComponent } from './components/soccer-league/new-soccer-league/new-soccer-league.component';
import { EditSoccerLeagueComponent } from './components/soccer-league/edit-soccer-league/edit-soccer-league.component';
import { SoccerLeagueDetailComponent } from './components/soccer-league/soccer-league-detail/soccer-league-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    SoccerLeagueListComponent,
    NewSoccerLeagueComponent,
    EditSoccerLeagueComponent,
    SoccerLeagueDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
