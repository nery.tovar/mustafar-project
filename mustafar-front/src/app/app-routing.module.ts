import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SoccerLeagueListComponent } from './components/soccer-league/soccer-league-list/soccer-league-list.component';
import { SoccerLeagueDetailComponent } from './components/soccer-league/soccer-league-detail/soccer-league-detail.component';
import { NewSoccerLeagueComponent } from './components/soccer-league/new-soccer-league/new-soccer-league.component';
import { EditSoccerLeagueComponent } from './components/soccer-league/edit-soccer-league/edit-soccer-league.component';

const routes: Routes = [
  {path: '', component: SoccerLeagueListComponent},
  {path: 'detail/:id', component: SoccerLeagueDetailComponent},
  {path: 'new', component: NewSoccerLeagueComponent},
  {path: 'edit/:id', component: EditSoccerLeagueComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
