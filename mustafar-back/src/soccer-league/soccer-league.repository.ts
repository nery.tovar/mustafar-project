import { EntityRepository, Repository } from "typeorm";
import { SoccerLeagueEntity } from './soccer-league.entity';

@EntityRepository(SoccerLeagueEntity)
export class SoccerLeagueRepository extends Repository<SoccerLeagueEntity> {

}