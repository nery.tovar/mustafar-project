import { Module } from '@nestjs/common';
import { SoccerLeagueService } from './soccer-league.service';
import { SoccerLeagueController } from './soccer-league.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SoccerLeagueEntity } from './soccer-league.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SoccerLeagueEntity])],
  providers: [SoccerLeagueService],
  controllers: [SoccerLeagueController]
})
export class SoccerLeagueModule {}
