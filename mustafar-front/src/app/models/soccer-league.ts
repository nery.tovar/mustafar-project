export class SoccerLeague {
  id?: number;
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
