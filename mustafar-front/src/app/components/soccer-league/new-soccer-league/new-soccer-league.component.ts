import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SoccerLeague } from 'src/app/models/soccer-league';
import { SoccerLeagueService } from 'src/app/services/soccer-league.service';

@Component({
  selector: 'app-new-soccer-league',
  templateUrl: './new-soccer-league.component.html',
  styleUrls: ['./new-soccer-league.component.css']
})
export class NewSoccerLeagueComponent implements OnInit {
  name = '';

  constructor(private soccerLeagueService: SoccerLeagueService, private toastrService: ToastrService, private router: Router) { }

  ngOnInit(): void {
  }

  createSoccerLeague(): void {
    const soccerLeague = new SoccerLeague(this.name);
    this.soccerLeagueService.createSoccerLeague(soccerLeague).subscribe(
      data => {
        this.toastrService.success('Liga de Fútbol registrada exitosamente', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      error => {
        this.toastrService.error(error.error.message, 'Fail', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
      }
    );
  }

}
