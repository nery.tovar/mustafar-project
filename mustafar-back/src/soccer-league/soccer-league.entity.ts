import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: 'soccer_leagues_mustafar'})
export class SoccerLeagueEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'varchar', length: 100, nullable: false})
  name: string;
}