import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SoccerLeagueEntity } from './soccer-league.entity';
import { SoccerLeagueRepository } from './soccer-league.repository';
import { SoccerLeagueDto } from './dto/soccer-league.dto';

@Injectable()
export class SoccerLeagueService {
  constructor(
    @InjectRepository(SoccerLeagueEntity) private soccerLeagueRepository: SoccerLeagueRepository
  ) {}

  async getSoccerLeagues(): Promise<SoccerLeagueEntity[]> {
    const listSoccerLeagues = await this.soccerLeagueRepository.find();

    if(!listSoccerLeagues.length) {
      throw new NotFoundException({message: 'No hay Ligas de Fútbol registradas.'});
    }

    return listSoccerLeagues;
  }

  async getSoccerLeagueById(id: number): Promise<SoccerLeagueEntity> {
    const soccerLeague = await this.soccerLeagueRepository.findOne(id);

    if(!soccerLeague) {
      throw new NotFoundException({message: 'No existe la Liga de Fútbol.'});
    }

    return soccerLeague;
  }

  async getSoccerLeagueByName(name: string): Promise<SoccerLeagueEntity> {
    const soccerLeague = await this.soccerLeagueRepository.findOne({name: name});

    return soccerLeague;
  }

  async createSoccerLeague(dto: SoccerLeagueDto): Promise<any> {
    const exists = await this.getSoccerLeagueByName(dto.name);
    const soccerLeague = this.soccerLeagueRepository.create(dto);

    if(exists) {
      throw new BadRequestException({message: 'El nombre de la Liga de Fútbol ya existe.'});
    }
    
    await this.soccerLeagueRepository.save(soccerLeague);

    return {message: `La Liga de Fútbol "${soccerLeague.name}" ha sido registrada correctamente.`};
  }

  async updateSoccerLeague(id: number, dto: SoccerLeagueDto): Promise<any> {
    const exists = await this.getSoccerLeagueByName(dto.name);
    const soccerLeague = await this.getSoccerLeagueById(id);

    if(!soccerLeague) {
      throw new BadRequestException({message: 'No existe la Liga de Fútbol.'});
    }

    if(exists && exists.id !== id) {
      throw new BadRequestException({message: 'El nombre de la Liga de Fútbol ya existe.'});
    }

    dto.name ? soccerLeague.name = dto.name : soccerLeague.name = soccerLeague.name;
    await this.soccerLeagueRepository.save(soccerLeague);

    return {message: `La Liga de Fútbol "${soccerLeague.name}" ha sido actualizada correctamente.`};
  }

  async deleteSoccerLeague(id: number): Promise<any> {
    const soccerLeague = await this.getSoccerLeagueById(id);
    await this.soccerLeagueRepository.delete(id);

    return {message: `La Liga de Fútbol "${soccerLeague.name}" ha sido eliminada correctamente.`};
  }
}
