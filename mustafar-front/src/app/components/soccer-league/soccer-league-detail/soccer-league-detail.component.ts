import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SoccerLeague } from '../../../models/soccer-league';
import { SoccerLeagueService } from '../../../services/soccer-league.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-soccer-league-detail',
  templateUrl: './soccer-league-detail.component.html',
  styleUrls: ['./soccer-league-detail.component.css']
})
export class SoccerLeagueDetailComponent implements OnInit {
  soccerLeague = new SoccerLeague('');

  constructor(private soccerLeagueService: SoccerLeagueService, private activatedRoute: ActivatedRoute, private toastrService: ToastrService, private router: Router) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.soccerLeagueService.getSoccerLeagueById(id).subscribe(
      data => {
        this.soccerLeague = data;
      },
      error => {
        this.toastrService.error(error.error.message, 'Fail', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.goBack();
      }
    );
  }

  goBack(): void {
    this.router.navigate(['/']);
  }

}
